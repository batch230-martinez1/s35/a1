const express = require("express");
//  Mongoose is a package tha allows creation of schemas model or data structures
//  Also has access to a number of methods for manipulating database
const mongoose = require("mongoose");


const app = express();
const port = 3001;

//  [SECTION]  MongoDB Connection
//  Connect to the database by passing in your connectin string, remember to replace the password and database names with actual values
// Syntax:
    // mongoose.connect("<MongoDB connection string>", {urlNewParser: true})
mongoose.connect("mongodb+srv://admin:admin@batch230.s8nzwea.mongodb.net/s35?retryWrites=true&w=majority", 
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
)

//  Connection to database
//  Allows to handle errors when the initial connection is establish
//  Works with the on and once Mongooose methods
let db = mongoose.connection

//  If a conncection error occured, output int the console
// console.error.bind(console) allows us to print error in the browser console and in the terminal
db.on("error", console.error.bind(console, "connsection error"));

db.once("open", ()=> console.log("We're connected to the cloud database"));

app.use(express.json());

const taskSchema = new mongoose.Schema({
  name : String,
  status :{
    type: String,
    default: "pending"
  }
})

const Task =  mongoose.model("Task", taskSchema);

app.post("/tasks", (request, response) =>{
//  Check if there are duplicate tasks
  Task.findOne({name: request.body.name}, (err, result) =>{
    
    if(result!=null && result.name == request.body.name){
    return response.send("Duplicate task found");
    }
    else{
      let newTask = new Task({
        name: request.body.name
      })
      newTask.save((saveErr, savedTask) =>{
        //  if an error is saved in saveErr parameter
        if(saveErr){
          return console.log(saveErr);
        }
        else{
          return response.status(201).send("New task created");
        }
      })
    }
  })
})  

app.get("/tasks", (request, res) =>{
  Task.find({}, (err, result) =>{
    if(err){
      return console.log(err);
    }
    else{
      return res.status(200).json({
         data : result
      })
    }
  })

})
  
const userSchema = new mongoose.Schema({
  username : String,
  password : String,
  status :{
    type: String,
    default: "pending"
  }
})


const User =  mongoose.model("User", userSchema);

app.post("/user", (request, response) =>{
    User.findOne({username: request.body.username}, (err, result) =>{
      
      if(result!=null && result.username == request.body.username){
      return response.send("Duplicate user found");
      }
      else{
        let newUser = new User({
          username: request.body.username,
          password: request.body.password
        })
        newUser.save((saveErr, savedTask) =>{
          //  if an error is saved in saveErr parameter
          if(saveErr){
            return console.log(saveErr);
          }
          else{
            return response.status(201).send("New user registered");
          }
        })
      }
    })
  })  


















app.listen(port, ()=>console.log(`Server running at port ${port}`));




